package com.hua.sk.echarts.component.part;



/**
 * @Description: 图形样式
 * @author Jhua
 * @version 1.0
 */
public class ItemStyle {
	/**
	 * 默认样式
	 */
	private NormalStyle normal =new NormalStyle();
	/**
	 * 强调样式
	 */
	private EmphasisStyle emphasis=new EmphasisStyle();

	public NormalStyle getNormal() {
		return normal;
	}

	public void setNormal(NormalStyle normal) {
		this.normal = normal;
	}

	public EmphasisStyle getEmphasis() {
		return emphasis;
	}

	public void setEmphasis(EmphasisStyle emphasis) {
		this.emphasis = emphasis;
	}

}
