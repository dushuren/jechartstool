package com.hua.sk.echarts.component.part;


import com.hua.sk.echarts.component.ToolTip;

/**
 * @Description: 饼图data
 * @author Jhua
 * @version 1.0
 */
public class PieData {

	private String name;
	private String value;
	private boolean selected = false;
	private Label label=new Label();
	private ItemStyle itemStyle = new ItemStyle();
	private ToolTip toolTip =new ToolTip();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public ItemStyle getItemStyle() {
		return itemStyle;
	}

	public void setItemStyle(ItemStyle itemStyle) {
		this.itemStyle = itemStyle;
	}

	public ToolTip getToolTip() {
		return toolTip;
	}

	public void setToolTip(ToolTip toolTip) {
		this.toolTip = toolTip;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
