package com.hua.sk.echarts.component.part;

import java.io.Serializable;

/**
 * @Description: 公共样式
 * @author Jhua
 * @version 1.0
 */
public class Style implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * [ default: 自适应 ]
	 */
	private String color;
	/**
	 * 柱条的描边颜色[ default: '#000' ]
	 */
	private String borderColor = "#000";
	/**
	 * 柱条的描边宽度，默认不描边[ default: 0 ]
	 */
	private int borderWidth = 0;
	/**
	 * 柱条的描边类型，默认为实线，[ default: 'solid' ] 支持 'dashed', 'dotted','solid'
	 */
	private String borderType = "solid";
	/**
	 * 圆角半径，单位px，支持传入数组分别指定 4 个圆角半径
	 */
	//private int barBorderRadius = 0;
	/**
	 * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果
	 */
	private String shadowBlur;
	/**
	 * 阴影颜色。支持的格式同
	 */
	private String shadowColor;
	/**
	 * 阴影水平方向上的偏移距离
	 */
	private int shadowOffsetX = 0;
	/**
	 * 阴影垂直方向上的偏移距离
	 */
	private int shadowOffsetY = 0;
	/**
	 * 图形透明度。支持从 0 到 1 的数字，为 0 时不绘制该图形
	 */
	private String opacity;
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getBorderColor() {
		return borderColor;
	}
	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}
	public int getBorderWidth() {
		return borderWidth;
	}
	public void setBorderWidth(int borderWidth) {
		this.borderWidth = borderWidth;
	}
	public String getBorderType() {
		return borderType;
	}
	public void setBorderType(String borderType) {
		this.borderType = borderType;
	}
	public String getShadowBlur() {
		return shadowBlur;
	}
	public void setShadowBlur(String shadowBlur) {
		this.shadowBlur = shadowBlur;
	}
	public String getShadowColor() {
		return shadowColor;
	}
	public void setShadowColor(String shadowColor) {
		this.shadowColor = shadowColor;
	}
	public int getShadowOffsetX() {
		return shadowOffsetX;
	}
	public void setShadowOffsetX(int shadowOffsetX) {
		this.shadowOffsetX = shadowOffsetX;
	}
	public int getShadowOffsetY() {
		return shadowOffsetY;
	}
	public void setShadowOffsetY(int shadowOffsetY) {
		this.shadowOffsetY = shadowOffsetY;
	}
	public String getOpacity() {
		return opacity;
	}
	public void setOpacity(String opacity) {
		this.opacity = opacity;
	}
	
	
}
