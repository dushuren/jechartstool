package com.hua.sk.echarts.component;

/**
 * @Description: 提示框组件
 * @author Jhua
 * @version 1.0
 */
public class ToolTip {

	private boolean show = true;
	/**
	 * 触发类型[ default: 'item' ] 可选: item图形触发，主要在散点图，饼图等无类目轴的图表中使用
	 * axis坐标轴触发，主要在柱状图，折线图等会使用类目轴的图表中使用 none什么都不触发
	 */
	private String trigger = "item";
	/**
	 * 提示框浮层内容格式器
	 */
	private String formatter;

	public ToolTip() {
	}

	public ToolTip(String trigger) {
		this.trigger = trigger;
	}

	public ToolTip(boolean show, String trigger) {
		this.show = show;
		this.trigger = trigger;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public String getTrigger() {
		return trigger;
	}

	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

	public String getFormatter() {
		return formatter;
	}

	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

}
