package com.hua.sk.echarts.component.part;

/**
 * @Description: 公共Label
 * @author Jhua
 * @version 1.0
 */
public class BaseLabel {
	/**
	 * [ default: false ]
	 */
	private boolean show = false;
	/**
	 * 标签的位置[ default: 'top' ]
	 */
	private String position = "top";
	/**
	 * 距离图形元素的距离。当 position 为字符描述值（如 'top'、'insideRight'）时候有效
	 */
	private int distance = 5;
	/**
	 * 标签旋转。从 -90 度到 90 度。正值是逆时针
	 */
	private int rotate;
	/**
	 * 是否对文字进行偏移。默认不偏移。例如：[30, 40] 表示文字在横向上偏移 30，纵向上偏移 40
	 */
	private String offset;
	/**
	 * 标签内容格式器，支持字符串模板和回调函数两种形式，字符串模板与回调函数返回的字符串均支持用 \n 换行
	 */
	private String formatter;
	/**
	 * 文字的颜色[ default: "#fff" ]
	 */
	private String color = "#fff";
	/**
	 * 文字字体的风格[ default: 'normal' ]
	 */
	private String fontStyle = "normal";
	/**
	 * 文字字体的粗细[ default: normal ] 可选 normal，bold，bolder，lighter，100 | 200 | 300 |
	 * 400...
	 */
	private String fontWeight = "normal";
	/**
	 * 文字的字体系列[ default: 'sans-serif' ]
	 */
	private String fontFamily = "sans-serif";
	/**
	 * 文字的字体大小[ default: 12 ]
	 */
	private int fontSize = 12;

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public int getRotate() {
		return rotate;
	}

	public void setRotate(int rotate) {
		this.rotate = rotate;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getFormatter() {
		return formatter;
	}

	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getFontStyle() {
		return fontStyle;
	}

	public void setFontStyle(String fontStyle) {
		this.fontStyle = fontStyle;
	}

	public String getFontWeight() {
		return fontWeight;
	}

	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

}
