package com.hua.sk.echarts.component.part;

/**
 * @Description: 启用功能还原设置
 * @author Jhua
 * @version 1.0
 */
public class Restore {
	private boolean show = false;

	public Restore() {

	}

	public Restore(boolean show) {
		this.show = show;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

}
