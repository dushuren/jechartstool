package com.hua.sk.echarts.component;

import com.hua.sk.echarts.component.part.BaseSeries;

/**
 * @Description: 柱状图/折线图 系列
 * @author Jhua
 * @version 1.0
 */
public class BarLineSeries extends BaseSeries {

	/**
	 * 使用的 x 轴的 index，在单个图表实例中存在多个 x 轴的时候有用。
	 */
	private int xAxisIndex = 0;
	/**
	 * 使用的 y 轴的 index，在单个图表实例中存在多个 y轴的时候有用。
	 */
	private int yAxisIndex = 0;
	/**
	 * 柱状图 数据堆叠，同个类目轴上系列配置相同的stack值可以堆叠放置[ default: null ]
	 */
	private String stack;
	private String[] data;

	public int getxAxisIndex() {
		return xAxisIndex;
	}

	public void setxAxisIndex(int xAxisIndex) {
		this.xAxisIndex = xAxisIndex;
	}

	public int getyAxisIndex() {
		return yAxisIndex;
	}

	public void setyAxisIndex(int yAxisIndex) {
		this.yAxisIndex = yAxisIndex;
	}

	public String getStack() {
		return stack;
	}

	public void setStack(String stack) {
		this.stack = stack;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

}
