package com.hua.sk.echarts.component.part;

/**
 * 分隔区域
 * 
 * @author Jhua
 * @version 1.0
 *
 */
public class SplitArea {
	/**
	 * 是否显示分隔区域[ default: false ]
	 */
	private boolean show = false;

	public SplitArea() {

	}

	public SplitArea(boolean show) {
		this.show = show;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

}
