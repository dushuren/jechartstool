package com.hua.sk.echarts.component.part;

/**
 * @author Jhua
 * @version 1.0
 * @Description: 仪表盘详情
 */
public class GaugeDetail {
    /**
     * 可以格式化文本
     */
    private String formatter = "{value}%";

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }
}
