package com.hua.sk.echarts.component.part;


/**
 * @author Jhua
 * @version 1.0
 * @Description: 公共 系列
 */
public class BaseSeries {

    /**
     * 系列名称
     */
    private String name;
    /**
     * 类型
     */
    private String type;
    /**
     * 文本标签
     */
    private Label label = new Label();
    /**
     * 图形样式
     */
    private ItemStyle itemStyle = new ItemStyle();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

}
