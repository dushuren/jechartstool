package com.hua.sk.echarts.component;


import com.hua.sk.echarts.component.part.Feature;

/**
 * @Description: 工具箱
 * @author Jhua
 * @version 1.0
 */
public class ToolBox {
	/**
	 * true（显示） | false（隐藏） ，默认false
	 */
	private boolean show = true;
	private String x = "right";
	private String y = "top";
	/**
	 * 工具栏 icon 的布局朝向[ default: 'horizontal' ] 可选:horizontal|vertical
	 */
	private String orient = "horizontal";
	/**
	 * 工具栏 icon 每项之间的间隔。横向布局时为水平间隔，纵向布局时为纵向间隔[ default: 10 ]
	 */
	private int itemGap = 10;
	/**
	 * 工具栏 icon 的大小[ default: 15 ]
	 */
	private int itemSize = 15;
	/**
	 * 是否在鼠标 hover 的时候显示每个工具 icon 的标题[ default: true ]
	 */
	private boolean showTitle = true;
	/**
	 * 自定义工具按钮
	 */
	private Feature feature = new Feature();

	public ToolBox() {

	}

	public ToolBox(boolean show) {
		this.show = show;
	}

	public ToolBox(boolean show, Feature feature) {
		this.show = show;
		this.feature = feature;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public String getOrient() {
		return orient;
	}

	public void setOrient(String orient) {
		this.orient = orient;
	}

	public int getItemGap() {
		return itemGap;
	}

	public void setItemGap(int itemGap) {
		this.itemGap = itemGap;
	}

	public int getItemSize() {
		return itemSize;
	}

	public void setItemSize(int itemSize) {
		this.itemSize = itemSize;
	}

	public boolean isShowTitle() {
		return showTitle;
	}

	public void setShowTitle(boolean showTitle) {
		this.showTitle = showTitle;
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

}
