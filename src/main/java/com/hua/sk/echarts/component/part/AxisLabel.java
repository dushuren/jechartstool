package com.hua.sk.echarts.component.part;

/**
 * 坐标轴刻度标签的相关设置
 * 
 * @author Jhua
 * @version 1.0
 */
public class AxisLabel {
	public AxisLabel() {

	}

	public AxisLabel(String formatter) {
		this.formatter = formatter;
	}

	public AxisLabel(String formatter, TextStyle textStyle) {
		this.formatter = formatter;
		this.textStyle = textStyle;
	}

	/**
	 * 文本样式（详见textStyle），其中当坐标轴为数值型和时间型时，color接受方法回调，实现个性化的颜色定义
	 */
	private TextStyle textStyle = new TextStyle("#333");
	/**
	 * 是否显示刻度标签[ default: true ]
	 */
	private boolean show = true;
	/**
	 * 标签显示挑选间隔，[ default: true ]， 可选为：'auto'（自动隐藏显示不下的） | 0（全部显示） |
	 * {number}（用户指定选择间隔） {function}函数回调，传递参数[index，data[index]]，返回true显示，返回false隐藏
	 */
	private String interval = "auto";
	/**
	 * 刻度标签是否朝内，默认朝外[ default: false ]
	 */
	private boolean inside = false;
	/**
	 * 标签旋转角度，[ default: 0 ]，不旋转，正值为逆时针，负值为顺时针，可选为：-90 ~ 90
	 */
	private int rotate = 8;
	/**
	 * 刻度标签与轴线之间的距离。[ default: 8 ]
	 */
	private int margin = 8;
	/**
	 * 间隔名称格式器：{string}（Template） | {Function}
	 */
	private String formatter;
	/**
	 * 是否显示最小 tick 的 label。[ default: null ] 可取值 true, false,
	 * null。默认自动判定（即如果标签重叠，不会显示最小 tick 的 label）。
	 */
	private String showMinLabel;
	/**
	 * 是否显示最大 tick 的 label。[ default: null ] 可取值 true, false,
	 * null。默认自动判定（即如果标签重叠，不会显示最大 tick 的 label）。
	 */
	private String showMaxLabel;
	/**
	 * 文字的字体系列[ default: 'sans-serif' ] 还可以是 'serif' , 'monospace', 'Arial',
	 * 'Courier New', 'Microsoft YaHei', ...
	 */
	private String fontFamily = "sans-serif";
	/**
	 * 文字垂直对齐方式，默认自动。 可选：'top','middle','bottom'
	 */
	private String verticalAlign = "auto";
	/**
	 * 行高
	 */
	private int lineHeight;
	/**
	 * 文字块背景色。[ default: 'transparent' ] 可以是直接的颜色值,可以支持使用图片
	 */
	private String backgroundColor;
	/**
	 * 文字块边框颜色[ default: 'transparent' ]
	 */
	private String borderColor = "transparent";
	/**
	 * 文字块边框宽度[ default: 0 ]
	 */
	private int borderWidth = 0;
	/**
	 * 文字块的圆角[ default: 0 ]
	 */
	private int borderRadius = 0;
	/**
	 * 文字块的内边距[ default: 0 ]。例如： padding: [3, 4, 5, 6]：表示 [上, 右, 下, 左] 的边距。 padding:
	 * 4：表示 padding: [4, 4, 4, 4]。 padding: [3, 4]：表示 padding: [3, 4, 3, 4]。 注意，文字块的
	 * width 和 height 指定的是内容高宽，不包含 padding。
	 */
	private int padding = 0;
	/**
	 * 文字块的背景阴影颜色[ default: 'transparent' ]
	 */
	private String shadowColor = "transparent";
	/**
	 * 文字块的背景阴影长度[ default: 0 ]
	 */
	private int shadowBlur = 0;
	/**
	 * 文字块的背景阴影 X 偏移[ default: 0 ]
	 */
	private int shadowOffsetX = 0;
	/**
	 * 文字块的背景阴影 Y 偏移[ default: 0 ]
	 */
	private int shadowOffsetY = 0;
	// private String width;
	// private String height;
	/**
	 * 文字本身的描边颜色[ default: 'transparent' ]
	 */
	private String textBorderColor = "transparent";
	/**
	 * 文字本身的描边宽度[ default: 0 ]
	 */
	private int textBorderWidth = 0;
	/**
	 * 文字本身的阴影颜色[ default: 'transparent' ]
	 */
	private String textShadowColor = "transparent";
	/**
	 * 文字本身的阴影长度[ default: 0 ]
	 */
	private int textShadowBlur = 0;
	/**
	 * 文字本身的阴影 X 偏移[ default: 0 ]
	 */
	private int textShadowOffsetX = 0;
	/**
	 * 文字本身的阴影 Y 偏移[ default: 0 ]
	 */
	private int textShadowOffsetY = 0;

	public TextStyle getTextStyle() {
		return textStyle;
	}

	public void setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public boolean isInside() {
		return inside;
	}

	public void setInside(boolean inside) {
		this.inside = inside;
	}

	public int getRotate() {
		return rotate;
	}

	public void setRotate(int rotate) {
		this.rotate = rotate;
	}

	public int getMargin() {
		return margin;
	}

	public void setMargin(int margin) {
		this.margin = margin;
	}

	public String getFormatter() {
		return formatter;
	}

	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

	public String getShowMinLabel() {
		return showMinLabel;
	}

	public void setShowMinLabel(String showMinLabel) {
		this.showMinLabel = showMinLabel;
	}

	public String getShowMaxLabel() {
		return showMaxLabel;
	}

	public void setShowMaxLabel(String showMaxLabel) {
		this.showMaxLabel = showMaxLabel;
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public String getVerticalAlign() {
		return verticalAlign;
	}

	public void setVerticalAlign(String verticalAlign) {
		this.verticalAlign = verticalAlign;
	}

	public int getLineHeight() {
		return lineHeight;
	}

	public void setLineHeight(int lineHeight) {
		this.lineHeight = lineHeight;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public String getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}

	public int getBorderWidth() {
		return borderWidth;
	}

	public void setBorderWidth(int borderWidth) {
		this.borderWidth = borderWidth;
	}

	public int getBorderRadius() {
		return borderRadius;
	}

	public void setBorderRadius(int borderRadius) {
		this.borderRadius = borderRadius;
	}

	public int getPadding() {
		return padding;
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}

	public String getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(String shadowColor) {
		this.shadowColor = shadowColor;
	}

	public int getShadowBlur() {
		return shadowBlur;
	}

	public void setShadowBlur(int shadowBlur) {
		this.shadowBlur = shadowBlur;
	}

	public int getShadowOffsetX() {
		return shadowOffsetX;
	}

	public void setShadowOffsetX(int shadowOffsetX) {
		this.shadowOffsetX = shadowOffsetX;
	}

	public int getShadowOffsetY() {
		return shadowOffsetY;
	}

	public void setShadowOffsetY(int shadowOffsetY) {
		this.shadowOffsetY = shadowOffsetY;
	}

	public String getTextBorderColor() {
		return textBorderColor;
	}

	public void setTextBorderColor(String textBorderColor) {
		this.textBorderColor = textBorderColor;
	}

	public int getTextBorderWidth() {
		return textBorderWidth;
	}

	public void setTextBorderWidth(int textBorderWidth) {
		this.textBorderWidth = textBorderWidth;
	}

	public String getTextShadowColor() {
		return textShadowColor;
	}

	public void setTextShadowColor(String textShadowColor) {
		this.textShadowColor = textShadowColor;
	}

	public int getTextShadowBlur() {
		return textShadowBlur;
	}

	public void setTextShadowBlur(int textShadowBlur) {
		this.textShadowBlur = textShadowBlur;
	}

	public int getTextShadowOffsetX() {
		return textShadowOffsetX;
	}

	public void setTextShadowOffsetX(int textShadowOffsetX) {
		this.textShadowOffsetX = textShadowOffsetX;
	}

	public int getTextShadowOffsetY() {
		return textShadowOffsetY;
	}

	public void setTextShadowOffsetY(int textShadowOffsetY) {
		this.textShadowOffsetY = textShadowOffsetY;
	}

}
