package com.hua.sk.echarts.component.part;

/**
 * @Description: 设置直线指示器
 * @author Jhua
 * @version 1.0
 */
public class LineStyle {
	private String color[][]; // 颜色
	private String width = "8";

	public LineStyle() {

	}

	public String[][] getColor() {
		return color;
	}

	public void setColor(String[][] color) {
		this.color = color;
	}

	public LineStyle(String[][] color) {
		this.color = color;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

}
