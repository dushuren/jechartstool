package com.hua.sk.echarts.component.part;

/**
 * @Description: 框选区域缩放
 * @author Jhua
 * @version 1.0
 */
public class DataZoom {
	private boolean show = false;
	private DataZoomTitle title = new DataZoomTitle();

	public DataZoom() {

	}

	public DataZoom(boolean show) {
		this.show = show;
	}

	public DataZoom(boolean show, DataZoomTitle title) {
		this.show = show;
		this.title = title;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public void setTitle(DataZoomTitle title) {
		this.title = title;
	}

	public boolean isShow() {
		return show;
	}

	public DataZoomTitle getTitle() {
		return title;
	}

}
