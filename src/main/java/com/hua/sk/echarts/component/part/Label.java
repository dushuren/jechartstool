package com.hua.sk.echarts.component.part;


/**
 * @Description: 图形上的文本标签 可用于说明图形的一些数据信息
 * @author Jhua
 * @version 1.0
 */
public class Label {

	private NormalLabel normal = new NormalLabel();
	private EmphasisLabel emphasis = new EmphasisLabel();

	public NormalLabel getNormal() {
		return normal;
	}

	public void setNormal(NormalLabel normal) {
		this.normal = normal;
	}

	public EmphasisLabel getEmphasis() {
		return emphasis;
	}

	public void setEmphasis(EmphasisLabel emphasis) {
		this.emphasis = emphasis;
	}

}
