package com.hua.sk.echarts.component;

import com.hua.sk.echarts.component.part.BaseSeries;
import com.hua.sk.echarts.component.part.PieData;

import java.util.List;

/**
 * @Description: 饼图 系列
 * @author Jhua
 * @version 1.0
 */
public class PieSeries extends BaseSeries {

	private List<PieData> data;
	/**
	 * 饼图的半径，数组的第一项是内半径，第二项是外半径
	 */
	private String[] radius = { "0", "75%" };

	public List<PieData> getData() {
		return data;
	}

	public void setData(List<PieData> data) {
		this.data = data;
	}

	public String[] getRadius() {
		return radius;
	}

	public void setRadius(String[] radius) {
		this.radius = radius;
	}

}
