package com.hua.sk.echarts.component.part;

/**
 * @Description: 保存图片
 * @author Jhua
 * @version 1.0
 */
public class SaveAsImage {
	public boolean isShow() {
		return show;
	}

	public SaveAsImage() {

	}

	public SaveAsImage(boolean show) {
		this.show = show;
	}

	public SaveAsImage(boolean show, int pixelRatio) {
		this.show = show;
		this.pixelRatio = pixelRatio;
	}

	public SaveAsImage(boolean show, int pixelRatio, String type) {
		this.show = show;
		this.pixelRatio = pixelRatio;
		this.type = type;
	}

	/**
	 * 是否显示该工具[ default: true ]
	 */
	private boolean show = true;
	/**
	 * 保存的图片格式 .支持 'png' 和 'jpeg'
	 */
	private String type = "png";
	/**
	 * 保存图片的分辨率比例，[ default: 1 ]默认跟容器相同大小，如果需要保存更高分辨率的，可以设置为大于 1 的值，例如 2
	 */
	private int pixelRatio = 2;

	public void setShow(boolean show) {
		this.show = show;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPixelRatio() {
		return pixelRatio;
	}

	public void setPixelRatio(int pixelRatio) {
		this.pixelRatio = pixelRatio;
	}

}
