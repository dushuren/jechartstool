package com.hua.sk.echarts.component;

/**
 * @Description: 图例
 * @author Jhua
 * @version 1.0
 */
public class Legend {
	private String x = "center"; // 水平安放位置，默认为全图居中，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）
	private String y = "top"; // 垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）
	private String orient = "horizontal"; // 布局方式，默认为水平布局，可选为：'horizontal' | 'vertical'
	private String data[];
	private boolean selectedMode = true; // 图例开关，默认true

	public Legend() {

	}

	public Legend(String[] data, boolean selectedMode) {
		this.data = data;
		this.selectedMode = selectedMode;
	}

	public Legend(String x, String y, String[] data) {
		this.x = x;
		this.y = y;
		this.data = data;
	}

	public Legend(String x, String y) {
		this.x = x;
		this.y = y;
	}

	public Legend(String x, String y, String orient, String[] data) {
		this.x = x;
		this.y = y;
		this.orient = orient;
		this.data = data;
	}

	public Legend(String x, String y, String orient) {
		this.x = x;
		this.y = y;
		this.orient = orient;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	public String getOrient() {
		return orient;
	}

	public void setOrient(String orient) {
		this.orient = orient;
	}

	public boolean isSelectedMode() {
		return selectedMode;
	}

	public void setSelectedMode(boolean selectedMode) {
		this.selectedMode = selectedMode;
	}

}
