package com.hua.sk.echarts.component;

import com.hua.sk.echarts.component.part.TextStyle;

/**
 * @Description: 标题
 * @author Jhua
 * @version 1.0
 */
public class Title {

	private boolean show = true;
	/**
	 * 主标题
	 */
	private String text;
	/**
	 * 副标题
	 */
	private String subtext;
	private String x = "left";
	private String y = "top";
	/**
	 * 主标题样式
	 */
	private TextStyle textStyle = new TextStyle();

	public Title() {

	}

	public Title(String text, TextStyle textStyle) {
		this.text = text;
		this.textStyle = textStyle;
	}

	public Title(String text, String x, String y, TextStyle textStyle) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.textStyle = textStyle;
	}

	public Title(boolean show, String text, String subtext, String x, String y, TextStyle textStyle) {
		this.show = show;
		this.text = text;
		this.subtext = subtext;
		this.x = x;
		this.y = y;
		this.textStyle = textStyle;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSubtext() {
		return subtext;
	}

	public void setSubtext(String subtext) {
		this.subtext = subtext;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public TextStyle getTextStyle() {
		return textStyle;
	}

	public void setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
	}

}
