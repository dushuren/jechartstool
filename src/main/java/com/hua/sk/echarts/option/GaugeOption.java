package com.hua.sk.echarts.option;

import com.hua.sk.echarts.component.GaugeSeries;
import com.hua.sk.echarts.component.Title;
import com.hua.sk.echarts.component.ToolBox;
import com.hua.sk.echarts.component.ToolTip;

import java.util.List;

/**
 * @Description: 仪表盘 option
 * @author Jhua
 * @version 1.0
 */
public class GaugeOption {
    private Title title;
    private ToolTip tooltip;
    private ToolBox toolbox;
    private List<GaugeSeries> series;

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public ToolTip getTooltip() {
        return tooltip;
    }

    public void setTooltip(ToolTip tooltip) {
        this.tooltip = tooltip;
    }

    public ToolBox getToolbox() {
        return toolbox;
    }

    public void setToolbox(ToolBox toolbox) {
        this.toolbox = toolbox;
    }

    public List<GaugeSeries> getSeries() {
        return series;
    }

    public void setSeries(List<GaugeSeries> series) {
        this.series = series;
    }
}
