package com.hua.sk.echarts.option;

import com.hua.sk.echarts.component.PieSeries;
import com.hua.sk.echarts.charts.component.data.Legend;
import com.hua.sk.echarts.component.*;
import java.util.List;

/**
 * @Description: 饼图 option
 * @author Jhua
 * @version 1.0
 */
public class PieOption {
	/**
	 * 是否启用拖拽重计算特性，默认启用
	 */
	private boolean calculable = false;
	/**
	 * 标题
	 */
	private Title title = new Title();
	/**
	 * 工具提示
	 */
	private ToolTip tooltip = new ToolTip();
	/**
	 * 工具箱
	 */
	private ToolBox toolbox = new ToolBox();
	/**
	 * 图例
	 */
	private Legend legend = new Legend();
	private List<PieSeries> series;

	public boolean isCalculable() {
		return calculable;
	}

	public void setCalculable(boolean calculable) {
		this.calculable = calculable;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public ToolTip getTooltip() {
		return tooltip;
	}

	public void setTooltip(ToolTip tooltip) {
		this.tooltip = tooltip;
	}

	public ToolBox getToolbox() {
		return toolbox;
	}

	public void setToolbox(ToolBox toolbox) {
		this.toolbox = toolbox;
	}

	public Legend getLegend() {
		return legend;
	}

	public void setLegend(Legend legend) {
		this.legend = legend;
	}

	public List<PieSeries> getSeries() {
		return series;
	}

	public void setSeries(List<PieSeries> series) {
		this.series = series;
	}

}
