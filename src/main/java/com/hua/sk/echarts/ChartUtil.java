package com.hua.sk.echarts;


import com.hua.sk.echarts.component.*;
import com.hua.sk.echarts.component.part.*;
import com.hua.sk.echarts.option.BarLineOption;
import com.hua.sk.echarts.option.GaugeOption;
import com.hua.sk.echarts.option.PieOption;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jhua
 * @version 1.0
 * @Description: 图表工具类
 */
public class ChartUtil {
    /**
     * 创建柱形图/折线图 option
     *
     * @param text
     * @param xAxisName
     * @param xData
     * @param yAxisName
     * @param yAxisUnit
     * @param dataList
     * @return
     */
    @SuppressWarnings("unchecked")
    public static BarLineOption createBarLine(String text, String xAxisName, String xData[], String yAxisName,
                                              String yAxisUnit, List<BarLineSeries> dataList) {
        String trigger = "axis";
        String x = "center";
        String y = "top";
        boolean isShow = true;
        String formatter = "{value}" + yAxisUnit;

        Map<String, Object> map = assembleLegendAndSeries(dataList);
        Title title = new Title();
        title.setText(text);
        // 设置工具提示
        ToolTip tooltip = new ToolTip();
        tooltip.setTrigger(trigger);
        // 设置工具栏
        ToolBox toolbox = new ToolBox();
        String type[] = {"line", "bar"};
        Feature feature = new Feature();
        feature.setMark(new Mark(true));
        feature.setDataView(new DataView(true));
        feature.setDataZoom(new DataZoom(false));
        feature.setMagicType(new MagicType(true, type));
        feature.setRestore(new Restore(true));
        feature.setSaveAsImage(new SaveAsImage(true));
        toolbox.setFeature(feature);
        toolbox.setShow(isShow);
        // 设置图例
        Legend legendBean = new Legend(x, y, (String[]) map.get("legend"));
        // 设置x轴
        XAxis xAxis = new XAxis();
        xAxis.setName(xAxisName);
        if (xData == null) {
            xData = new String[1];
            xData[0] = "";
        }
        xAxis.setData(xData);
        // 设置y轴
        YAxis yAxis = new YAxis();
        AxisLabel axisLabel = new AxisLabel(formatter);
        yAxis.setAxisLabel(axisLabel);
        yAxis.setName(yAxisName);

        BarLineOption option = new BarLineOption();
        option.setLegend(legendBean);
        option.setToolbox(toolbox);
        option.setxAxis(xAxis);
        option.setyAxis(yAxis);
        option.setTooltip(tooltip);
        option.setSeries((List<BarLineSeries>) map.get("seriesDataList"));

        return option;
    }

    /**
     * 封装 柱形图/折线图 series和legend
     *
     * @param dataList
     * @return
     */
    private static Map<String, Object> assembleLegendAndSeries(List<BarLineSeries> dataList) {
        Map<String, Object> map = new HashMap<>();

        List<BarLineSeries> seriesDataList = new ArrayList<>();
        if (dataList != null && dataList.size() > 0) {
            // 图例
            String legend[] = new String[dataList.size()];
            for (int i = 0; i < dataList.size(); i++) {
                legend[i] = dataList.get(i).getName();
                BarLineSeries series = new BarLineSeries();
                series.setData(dataList.get(i).getData());
                series.setName(dataList.get(i).getName());
                series.setStack(dataList.get(i).getStack());
                series.setType(dataList.get(i).getType());
                series.setxAxisIndex(dataList.get(i).getxAxisIndex());
                series.setyAxisIndex(dataList.get(i).getyAxisIndex());
                series.setItemStyle(dataList.get(i).getItemStyle());
                series.setLabel(dataList.get(i).getLabel());
                seriesDataList.add(series);
            }
            map.put("legend", legend);
        } else {
            BarLineSeries seriesBean = new BarLineSeries();
            seriesBean.setData(new String[]{});
            seriesDataList.add(seriesBean);
            map.put("legend", new String[]{});
        }
        map.put("seriesDataList", seriesDataList);
        return map;
    }

    /**
     * 饼图
     *
     * @param text 标题
     * @param fontSize 字体大小
     * @param x 图例水平安放位置，默认为全图居中，可选为：'center' | 'left' | 'right'
     * @param y 图例垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center'
     * @param radius
     * @param data 数据
     * @return
     */
    public static PieOption createPie(String text, int fontSize, String x, String y, String radius,
                                      List<PieData> data) {
        String radiusArray[] = new String[2];
        radiusArray[0] = "0%";
        radiusArray[1] = radius;
        return createPieAndRing(text, fontSize, x, y, radiusArray, data);
    }

    /**
     * 创建环形图
     *
     * @param text        标题
     * @param fontSize    字体大小
     * @param x           图例水平安放位置，默认为全图居中，可选为：'center' | 'left' | 'right'
     * @param y           图例垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center'
     * @param innerRadius 内半径，格式20%
     * @param outerRadius 外半径，格式30%
     * @param data        环形图数据
     */
    public static PieOption createRing(String text, int fontSize, String x, String y, String innerRadius,
                                       String outerRadius, List<PieData> data) {
        String radiusArray[] = new String[2];
        radiusArray[0] = innerRadius;
        radiusArray[1] = outerRadius;
        return createPieAndRing(text, fontSize, x, y, radiusArray, data);
    }

    /**
     * 饼图和环形图生成json
     *
     * @param text        标题
     * @param fontSize    字体大小
     * @param x           图例水平安放位置，默认为全图居中，可选为：'center' | 'left' | 'right'
     * @param y           图例垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center'
     * @param radiusArray 半径数组，[内半径，外半径] 格式{"50%", "70%"}
     */
    private static PieOption createPieAndRing(String text, int fontSize, String x, String y, String radiusArray[],
                                              List<PieData> data) {
        PieOption option = new PieOption();
        // 设置标题
        Title title = new Title();
        title.setText(text);
        TextStyle textStyle = new TextStyle();
        textStyle.setFontSize(fontSize);
        option.setTitle(title);
        // 设置工具提示
        ToolTip tooltip = new ToolTip();
        tooltip.setFormatter("{a} <br/>{b} : {c} ({d}%)");
        tooltip.setTrigger("item");
        option.setTooltip(tooltip);
        // 设置工具栏
        ToolBox toolbox = new ToolBox(true);
        String type[] = {"pie", "funnel"};
        Feature feature = new Feature(new Mark(false), new DataView(true), new MagicType(true, type), new Restore(true),
                new SaveAsImage(true));
        toolbox.setFeature(feature);
        option.setToolbox(toolbox);
        // 设置图例
        Legend legendBean = new Legend(x, y);
        String[] legendData = new String[data.size()];
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                legendData[i] = data.get(i).getName();
            }
        }
        legendBean.setData(legendData);
        option.setLegend(legendBean);
        List<PieSeries> series = new ArrayList<>();
        PieSeries ringSeries = new PieSeries();

        // 数据
        ringSeries.setName(text);
        ringSeries.setType("pie");
        ringSeries.setRadius(radiusArray);
        ringSeries.setData(data);
        series.add(ringSeries);
        option.setSeries(series);
        return option;
    }

    /**
     * 创建仪表盘
     *
     * @param text     仪表盘文本名称
     * @param fontSize 字体大小
     * @param value    仪表盘的值
     */
    public static GaugeOption createGauge(String text, int fontSize, String value) {
        GaugeOption option = new GaugeOption();
        //设置标题
        Title title = new Title();
        title.setText(text);
        TextStyle textStyle = new TextStyle();
        textStyle.setFontSize(fontSize);
        title.setTextStyle(textStyle);
        option.setTitle(title);
        //设置工具提示
        ToolTip tooltip = new ToolTip();
        tooltip.setFormatter("{c}%");
        option.setTooltip(tooltip);
        //设置工具栏
        ToolBox toolbox = new ToolBox();
        option.setToolbox(toolbox);
        //设值
        List<GaugeSeries> series = new ArrayList<>();
        GaugeSeries gaugeSeries = new GaugeSeries();
        GaugeDetail detail = new GaugeDetail();
        detail.setFormatter("{value}%");
        gaugeSeries.setName("");
        gaugeSeries.setDetail(detail);
        String data[] = new String[1];
        data[0] = value;
        gaugeSeries.setData(data);
        gaugeSeries.setType("gauge");
        series.add(gaugeSeries);
        option.setSeries(series);
        return option;
    }
}
