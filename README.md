# jechartstool
用java封装的echarts工具类  
可以轻松实现柱状图、折线图、饼图、堆积图、柱线混合图、仪表盘等常用图形  
以下例子使用kotlin写的，可以参考
---
    /**
     * 普通饼图
     */
    @RequestMapping("/getpie")
    fun getPie(): Map<String, Any> {
        val map = HashMap<String, Any>()
        val data = ArrayList<PieData>()
        val piedata = PieData()
        piedata.name = "名称1"
        piedata.value = "500"
        data.add(piedata)
        val piedata2 = PieData()
        piedata2.name = "名称2"
        piedata2.value = "1000"
        data.add(piedata2)
        map.put("option", ChartUtil.createPie("标题", 12, "center", "top", "50%", data))
        return map
    }

    /**
     * 环形饼图
     */
    @RequestMapping("/getRing")
    fun getPie1(): Map<String, Any> {
        val map = HashMap<String, Any>()
        val data = ArrayList<PieData>()
        val piedata = PieData()
        piedata.name = "名称1"
        piedata.value = "500"
        data.add(piedata)
        val piedata2 = PieData()
        piedata2.name = "名称2"
        piedata2.value = "1000"
        data.add(piedata2)
        map.put("option", ChartUtil.createRing("标题", 12, "center", "top", "20%", "30%", data))
        return map
    }

    /**
     * 叠加
     */
    @RequestMapping(value = "/kiByRisk")
    fun kiByRisk(): Map<String, Any> {
        val map = HashMap<String, Any>()
        val xData = arrayOf("红灯", "黄灯", "绿灯")
        val dataList = ArrayList<BarLineSeries>()
        val bardata1 = BarLineSeries()

        val data1 = arrayOf("20", "60")
        bardata1.data = data1
        bardata1.name = "红灯"
        bardata1.stack = "123"
        bardata1.type="bar"
        val itemStyle1 = ItemStyle()
        val normal1 = NormalStyle()
        normal1.color = "#FF0000"
        itemStyle1.normal = normal1


        val bardata2 = BarLineSeries()
        val data2 = arrayOf("10")
        bardata2.data = data2
        bardata2.name = "黄灯"
        bardata2.stack = "123"
        bardata2.type="bar"
        val normal2 = NormalStyle()
        normal2.color = "#FFFF00"
        val itemStyle2 = ItemStyle()
        itemStyle2.normal = normal2

        val bardata3 = BarLineSeries()
        val data3 = arrayOf("40","33","11")
        bardata3.data = data3
        bardata3.name = "绿灯"
        bardata3.stack = "123"
        bardata3.type="line"
        val normal3 = NormalStyle()
        normal3.color = "#00FF00"
        val itemStyle3 = ItemStyle()
        itemStyle3.normal = normal3
        dataList.add(bardata1)
        dataList.add(bardata2)
        dataList.add(bardata3)
        map.put("option", ChartUtil.createBarLine("123", "", xData, "111", "", dataList))
        return map
    }

    /**
     * 折线
     */
    @RequestMapping("/getLine")
    fun getLine(): Map<String, Any> {
        val map = HashMap<String, Any>()
        val xData = arrayOf("2014", "2015", "2016", "2017")
        //    val dataList = ArrayList<BarSeriesData>()
        val dataList = ArrayList<BarLineSeries>()
        val bardata1 = BarLineSeries()
        val data1 = arrayOf("20", "60", "22", "2")
        bardata1.data = data1
        bardata1.name = "abc"
        bardata1.type="line"
        //   bardata1.type="line"
        val label = Label()
        val nl = NormalLabel()
        nl.isShow = true
        label.normal = nl


        dataList.add(bardata1)
        map.put("option", ChartUtil.createBarLine("1234", "", xData, "111", "", dataList))

        return map
    }
---